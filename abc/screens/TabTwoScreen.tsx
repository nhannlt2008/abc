import React, { useState, useEffect } from 'react';
import { Button, StyleSheet } from 'react-native';

import EditScreenInfo from '../components/EditScreenInfo';
import { Text, View } from '../components/Themed';
import { useDispatch, useSelector } from "react-redux";
import { TouchableOpacity } from 'react-native-gesture-handler';
import * as action from '../reduxstore/index/index';
import Colors from '../constants/Colors';
import { FontAwesome5 } from '@expo/vector-icons';

export default function TabTwoScreen() {
  const Total = useSelector(state => state.TotalPayLoan);
  const ListPay = useSelector(state => state.ListPayLoan);
  const moneyPerWeek = useSelector(state => state.AmountPayLoan);

  const dispatch = useDispatch();
  const [week, setWeek] = useState(1);
  const [totalRemainder, setTotalRemainder] = useState(-1);

  useEffect(() => {    // Update the document title using the browser API   
    var value = 0;
    for (let i = 0; i < ListPay.length; i++) {
      value += ListPay[i].price;
    }
    value = Total - value;
    setTotalRemainder(value > 0 ? value : 0);
  });
  return (
    <View style={styles.container}>
      {/* <Text style={styles.title}>
        Total Repay Need: {totalRemainder == -1 ? '' : totalRemainder} {'\n'}</Text> */}
      <Text style={styles.title}>Week paying:{'\n'}</Text>

      <View style={{ flexDirection: 'row', marginVertical: 24 }}>
        <TouchableOpacity onPress={() => { if (week > 1) { setWeek(week - 1) } }}>
          <FontAwesome5 name="minus" size={24} color={Colors.blue} />
        </TouchableOpacity>
        <Text style={styles.title}>{week}</Text>
        <TouchableOpacity onPress={() => { if ((week + 1) * moneyPerWeek <= totalRemainder) { setWeek(week + 1) } }}>
          <FontAwesome5 name="plus" size={24} color={Colors.blue} />
        </TouchableOpacity>
      </View>
      <Text>Total MoneyPay: {week * moneyPerWeek}</Text>
      <TouchableOpacity style={styles.button} onPress={() => dispatch(action.pushPayToServer(week))}>
          <Text style={styles.btnText}>Repay Money</Text>
      </TouchableOpacity>
    </View>
  );
}
// const mapDispatchToProps = { payLoan };
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    marginHorizontal: 26
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
  btnText: {
    color: Colors.white,
    fontSize: 16
  },
  button: {
    marginTop: 40,
    backgroundColor: Colors.blue,
    paddingHorizontal: 36,
    paddingVertical: 12,
    borderRadius: 12
  }
});
