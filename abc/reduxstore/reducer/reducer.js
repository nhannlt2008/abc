import {Pay_Loan, Update_Loan} from '../action/actionload';
const initialState = {
    TotalPayLoan : 3600,
    AmountPayLoan : 300,
    ListPayLoan :[{
        loanId: 'L100X240322',
        date: '20-10-2020',
        price: 300,
        times: 5,
        totalTimes: 10,
      },
      {
        loanId: 'L100X240322',
        date: '20-10-2020',
        price: 300,
        times: 4,
        totalTimes: 10,
      },
      {
        loanId: 'L100X240322',
        date: '20-10-2020',
        price: 600,
        times: 3,
        totalTimes: 10,
      },
      {
        loanId: 'L100X240322',
        date: '20-10-2020',
        price: 300,
        times: 2,
        totalTimes: 10,
      },
      {
        loanId: 'L100X240322',
        date: '20-10-2020',
        price: 300,
        times: 1,
        totalTimes: 10,
      }]
}
export const payLoanReducer = (state = initialState,action)=>{
    switch(action.type){
        case Pay_Loan:
            const newList = state.ListPayLoan;
            newList.push(
                {
                    loanId: Math.floor(Math.random() * 100000) + 1 ,
                    date: '20-10-2021',
                    price: state.AmountPayLoan * action.amountWeek,
                    times: 1,
                    totalTimes: 10}
                );
            return {
                ...state,
                ListPayLoan: newList,
                // TotalPayLoan: state.TotalPayLoan -  state.AmountPayLoan ,
            }
        case Update_Loan:
            return {
                ...state,
                TotalPayLoan: state.TotalPayLoan - 1
            }
    }
    return state

}
